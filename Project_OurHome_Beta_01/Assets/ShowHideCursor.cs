﻿using UnityEngine;
using System.Collections;

public class ShowHideCursor : MonoBehaviour
{
    public bool showCursor = true;
    public bool ToggleCursor
    {
        get { return showCursor; }
        set { showCursor = value; Cursor.visible = showCursor; }
    }

    void Start()
    {
        ToggleCursor = showCursor;
    }

    void OnApplicationFocus(bool focus)
    {
        if (focus)
            StartCoroutine(LockCursor());
    }

    IEnumerator LockCursor()
    {
        yield return new WaitForSeconds(0.1f);
        ToggleCursor = showCursor;
    }
}
