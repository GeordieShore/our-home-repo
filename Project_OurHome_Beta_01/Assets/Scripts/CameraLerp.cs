﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;


public class CameraLerp : MonoBehaviour {

    public Transform endTransform;
    public AnimationCurve lerpCurve;


    private Vector3 startPos;
    private Vector3 targetPos;
    private Quaternion startRot;
    private Quaternion targetRot;

    private bool isLerping = false;
    private float lerpValue = 0.0f; //between 0 and 1
    private float lerpRotValue = 0.0f;
    public float lerpDuration = 2.0f;
    public float rotationDuration = 3.0f;

    private float curvedLerpValue = 0.0f;
	// Use this for initialization
	void Start () {
        StartLerp();
	}
	
	// Update is called once per frame
	void LateUpdate () {
		if (transform.position.x >= endTransform.transform.position.x - 1) 
		{
			SceneManager.LoadScene (0);
		}
	
        if(isLerping)
        {
            lerpValue += Time.deltaTime;
            lerpRotValue += Time.deltaTime;

            curvedLerpValue = lerpCurve.Evaluate( lerpValue / lerpDuration );

            transform.position = Vector3.Lerp(startPos, targetPos, curvedLerpValue);
            transform.rotation = Quaternion.Slerp(startRot, targetRot, curvedLerpValue);

            if (lerpValue >= lerpDuration && lerpRotValue >= rotationDuration)
                isLerping = false;

            if (transform.position.x > endTransform.position.x - 1)
            {
                Application.LoadLevel(0);
            }
        }
	}

    void StartLerp()
    {
        startPos = transform.position;
        targetPos = endTransform.position;
        isLerping = true;
        startRot = transform.rotation;
        targetRot = endTransform.rotation;


    }
}
