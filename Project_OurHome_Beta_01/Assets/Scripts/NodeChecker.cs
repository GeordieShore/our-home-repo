﻿using UnityEngine;
using System.Collections;

public class NodeChecker : MonoBehaviour {

    public bool Useable = false;

    public bool UseableWhenObstructed = true;
    public bool jumpDownNode = false;
    public Vector3 pos;
    public Vector3 desiredOffset;
    public Vector3 boxSize;
    private Quaternion boxOrientation;
    public string otherObjectTag = "Pushable";
    public Collider[] objectsColliding;

    // Use this for initialization
    void Start () {
        boxOrientation.eulerAngles = Vector3.zero;
        if (jumpDownNode == false)
        {
            //desiredOffset = new Vector3(0.6f, 0.4f, 0);
        }
        else
        {
            //desiredOffset = new Vector3(1, -0.5f, 0);
        }
            boxSize = new Vector3(0.001f, 0.001f, 0.001f);
	}
	
	// Update is called once per frame
	void Update ()
    {
        pos = transform.position;
        objectsColliding = Physics.OverlapBox(pos + transform.rotation * desiredOffset, boxSize, boxOrientation);


       if (objectsColliding.Length != 0)
        {
            // if (objectsColliding)
            //if one of those objects is tagged correctly
            foreach (Collider c in objectsColliding)
            {
				BoxCollider box = c.GetComponent<BoxCollider> ();
                if (box && box.isTrigger == true)
                    continue;
				if (c.tag == "Pushable" || c.tag == "Interactable") {

					Useable = UseableWhenObstructed;
					break;
				} else if (c.tag == "Wall") 
				{
					Useable = false;
				}

            }
        }
        else
        {
            Useable = !UseableWhenObstructed;
        }
    }

    void OnDrawGizmos()
    {

        Gizmos.DrawCube(pos + transform.rotation * desiredOffset, boxSize);

    }
}
