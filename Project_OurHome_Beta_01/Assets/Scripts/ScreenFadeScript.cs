﻿using UnityEngine;
using System.Collections;

/// <summary>
/// 
/// 
/// This code was borrowed from https://unity3d.com/learn/tutorials/topics/graphics/fading-between-scenes
/// 
/// </summary>

public class ScreenFadeScript : MonoBehaviour {

    public Texture2D fadeOutTexture; //the texture that will overlay the screen. this can be a black image or a loading 
    public float fadeSpeed = 0.8f; // the fading speed

    private int drawDepth = -1000; // the texture's order in the draw hierarchy: a low number means it renders on 
    private float alpha = 1.0f; // the textures alpha value between 0 and 1
    private int fadeDir = -1; // the direction to fade: in = -1 or out = 1

    void OnGUI()
    {
        //fade in out the alpha value using a direction, a speed and Time.deltatime to covnert the operation to seconds
        alpha += fadeDir * fadeSpeed * Time.deltaTime;
        // force (clamp) the number between 0 and 1 because GUI.color uses alpha values between 0 and one
        alpha = Mathf.Clamp01(alpha);

        // set color of our GUI (in this case our texture). all color values remain the same and the alpha is set to the alpha variable
        GUI.color = new Color(GUI.color.r, GUI.color.g, GUI.color.b, alpha);
        GUI.depth = drawDepth;
        GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), fadeOutTexture);
    }

    //sets fadeDir tot he direction parameter makingt he scene fade in or out
    public float BeginFade(int direction)
    {
        fadeDir = direction;
        return (fadeSpeed); // return the fadeSpeed variable so its easy to time the application.loadlevel();
    }

    //OnLevelWasLoaded is called when a level is loaded. It takes loaded level index (int) as a parameter so you can limit the fade in to certain scenes
    void OnLevelWasLoaded()
    {
        //alpha = 1; use this if the alpha is not set to 1 by default
        BeginFade(-1); //call the fade in function
    }
}
