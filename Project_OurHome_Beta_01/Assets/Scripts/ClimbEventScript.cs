﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class ClimbEventScript : BaseEventSystemScript
{

    public List<InteractableObject> jumpPositionList = new List<InteractableObject>();
    //InteractableObject[] jumpPositionArray; 

    public override void Start()
    {
        base.Start();
        // burrowTimer = burrowTimerStart;
        StartAction.AddListener(OnStartClimb);
    }

    // Update is called once per frame
    public override void Update()
    {
        base.Update();


        RecalculateCurrentObject();
    }

    public override void StartInteraction()
    {
        //base.StartInteraction();
        RecalculateCurrentObject();
        if (currentObject.objectType == InteractableType.CLIMB)
        {
			AudioManager.PlaySound(SoundType.CATJUMP);
            NodeChecker nodeScript = currentObject.GetComponent<NodeChecker>();

            if (nodeScript == null || nodeScript.Useable)
            {
                StartAction.Invoke();
                transform.position = currentObject.interactTransform.transform.position;
                EndAction.Invoke();
            }
        }
    }


    public override void OnTriggerEnter(Collider other)
    {
        //if you enter a push pull object, do this
        InteractableObject iObject = other.GetComponent<InteractableObject>();
        if (iObject && iObject.objectType == InteractableType.CLIMB)
        {
            // base.OnTriggerEnter(other);
            if (other.tag == "Interactable")
            {
                eventPossible = (1 << other.gameObject.layer) == collidableLayer.value;

                if (iObject != null)
                {
                    jumpPositionList.Add(iObject);
                    // RecalculateCurrentObject();
                }
            }
        }
    }

    public override void OnTriggerExit(Collider other)
    {
        //if you enter a push pull object, do this
        InteractableObject iObject = other.GetComponent<InteractableObject>();
        if (iObject && iObject.objectType == InteractableType.CLIMB)
        {
            jumpPositionList.Remove(iObject);
            //RecalculateCurrentObject();
            if (jumpPositionList.Count == 0)
            {
                eventPossible = false;
            }
        }

    }

    private void RecalculateCurrentObject()
    {
        if (jumpPositionList.Count == 0)
        {
            currentObject = null;
            return;
        }


        List<InteractableObject> correctAngleObjects = new List<InteractableObject>();


        //check angle between all objects in jumpPositionList, add if angle is less than certain threshold 30 degrees

        // store player forward vector

        //foreach interactable in jumppositionlist
        foreach (InteractableObject c in jumpPositionList)
        {
            //Vector3 fakeCatPos = transform.position - transform.forward * 0.2f;

            //directionBetween = c.transform.position - fakeCatPos;
            //// get vector from player to interactableObject
            //Debug.Log(directionBetween);

            float currAngle = Vector3.Angle(transform.forward, c.DodgyForward);


            if (currAngle < 60)
            {
                // Debug.Log(c.name);
                correctAngleObjects.Add(c);
            }
            //if Vector3.Angle(playerForward, vecToInteractable) < 30
            //add interactableObject to correctAngleObjects
        }



        //store float for minDist set to floatMAX
        float minDistance = float.MaxValue;
        //store int for index of closest
        int closestPoint = 0;

        for (int i = 0; i < correctAngleObjects.Count; i++)
        {
            //calculate distance to current position (i)
            float currdistance = 0;
            currdistance = (correctAngleObjects[i].transform.position - transform.position).sqrMagnitude;
            //if currdistance < mindistance
            if (currdistance < minDistance)
            {
                NodeChecker nodeScript = correctAngleObjects[i].GetComponent<NodeChecker>();

                if (nodeScript == null || nodeScript.Useable)
                {
                    minDistance = currdistance;
                    closestPoint = i;
                }
            }
        }

        if (correctAngleObjects.Count != 0)
            currentObject = correctAngleObjects[closestPoint];

    }

    void OnStartClimb()
    {
        //Debug.Log("Start C");
    }

}
