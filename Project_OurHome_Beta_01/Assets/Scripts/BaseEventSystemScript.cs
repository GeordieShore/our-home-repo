﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;

public class BaseEventSystemScript : MonoBehaviour
{
	[HideInInspector]
	public static string CatInputButton = "CatInput";
	[HideInInspector]
	public static string WolfInputButton = "WolfInput";

    public bool eventPossible;
    public bool triggerEvent;
    public bool triggerOnce;
    public InteractableObject currentObject;
    public bool isCat;
    public LayerMask collidableLayer;
    public ControllerV5 playerController;
    public bool Interacting = false;

    // -------- Ethan
    public bool isRepositioning = false;
    [HideInInspector]
    public Vector3 StartTarget;
    [HideInInspector]
    public Vector3 EndTarget;
    public float RepositionSpeed = 0.01f;
    public float RedirectionSpeed = 180f;
    [HideInInspector]
    public bool hasDoneInitalRot = false;
    [HideInInspector]
    public bool hasDoneMovement = false;
    [HideInInspector]
    public UnityEvent StartReposition = new UnityEvent();
    [HideInInspector]
    public UnityEvent UpdateReposition = new UnityEvent();
    [HideInInspector]
    public UnityEvent EndReposition = new UnityEvent();
    public UnityEvent StartAction = new UnityEvent();
    public UnityEvent EndAction = new UnityEvent();
    // --------

    //public AudioManager audioManagerScript;

    public virtual void Start()
    {
        // -------- Ethan
        StartReposition.AddListener(OnStartReposition);
        UpdateReposition.AddListener(OnUpdateReposition);
        EndReposition.AddListener(OnEndReposition);
        StartAction.AddListener(OnStartAction);
        EndAction.AddListener(OnEndAction);
        // --------

        //GameObject audioManagerObject = GameObject.FindGameObjectsWithTag("Audio Manager")[0];
        //audioManagerScript = audioManagerObject.GetComponent<AudioManager>();

        //need to get this and set it in the unity window
        if (!playerController) playerController = GetComponent<ControllerV5>();
    }

    public virtual void Update()
    {
        // -------- Ethan
        if (isRepositioning)
            UpdateReposition.Invoke();
        // --------

        //this controls whether or not push or any of the other mechanics are being used.
        if (triggerOnce == true)
            PlayerMovementAction();
        else if (triggerOnce == false)
            ObjectMovementAction();
    }



    public virtual void PlayerMovementAction()
    {
        if (isCat == true)
        {
            if (eventPossible == true && !playerController.LockInput && Input.GetButtonDown(CatInputButton))
            {
                //do things with currentObject if it's not null
                if (currentObject != null)
                {
                    StartInteraction();
                }
            }
        }
        else if (isCat == false)
        {
            if (eventPossible == true && !playerController.LockInput && Input.GetButtonDown(WolfInputButton))
            {
                //do things with currentObject if it's not null
                if (currentObject != null)
                {
                    StartInteraction();
                }
            }
        }
    }

    void ObjectMovementAction()
    {
        if (isCat == true)
        {
            if (eventPossible == true && !playerController.LockInput && Input.GetButton(CatInputButton))
            {
                //do things with currentObject if it's not null
                if (currentObject != null)
                {
                    StartInteraction();
                }

            }
        }
        else if (isCat == false)
        {
            if (eventPossible == true && !playerController.LockInput && Input.GetButton(WolfInputButton))
            {
                //do things with currentObject if it's not null
                if (currentObject != null)
                {
                    //must make sure to start update and end the interactions
                    if (Interacting == false)
                    {
                        if (playerController.currentlyInteracting == false)
                            StartInteraction();
                    }
                    UpdateInteraction();
                }

            }
            else
            {
                if (Interacting == true)
                    EndInteraction();
            }

        }
    }

    public virtual void StartInteraction()
    {
        playerController.currentlyInteracting = true;
        Interacting = true;
    }
    public virtual void UpdateInteraction()
    {

    }

    public virtual void EndInteraction()
    {
        playerController.currentlyInteracting = false;
        Interacting = false;
    }

    public virtual void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Interactable")
        {
            eventPossible = (1 << other.gameObject.layer) == collidableLayer.value;

            InteractableObject iObject = other.GetComponent<InteractableObject>();
            if (iObject != null)
            {
                currentObject = iObject;
            }
        }

    }

    public virtual void OnTriggerExit(Collider other)
    {
//        InteractableObject iObject = other.GetComponent<InteractableObject>();

        if (other.GetComponent<InteractableObject>() == currentObject)
        {
            eventPossible = false;
        }
    }
    // -------- Ethan
    #region ActionEvents
    public void OnStartReposition()
    {
        // be given targets
        if (StartTarget != Vector3.zero && EndTarget != Vector3.zero)
        {
            playerController.LockPhysics = true;
            playerController.LockInput = true;
            isRepositioning = true;
        }
    }

    public void OnUpdateReposition()
    {
        // move towards StartTarget
        Vector3 yStartTargetPos = StartTarget;
        yStartTargetPos.y = playerController.transform.position.y;

        Vector3 yEndTargetPos = EndTarget;
        yEndTargetPos.y = playerController.transform.position.y;

        // Movement
        if (!hasDoneMovement && playerController.transform.position != yStartTargetPos)
            playerController.transform.position = Vector3.MoveTowards(playerController.transform.position, yStartTargetPos, RepositionSpeed);
        else hasDoneMovement = true;

        // Rotation
        if (!hasDoneMovement)
        {
            if (!hasDoneInitalRot)
            {
                // goal
                Quaternion dir = Quaternion.LookRotation((yStartTargetPos - playerController.transform.position).normalized, playerController.transform.up);
                // progress
                Quaternion rot = Quaternion.RotateTowards(playerController.transform.rotation, dir, Time.deltaTime * RedirectionSpeed);
                playerController.transform.rotation = rot;
                float diff = Quaternion.Angle(rot, dir);
                if (diff > -1f && diff < 1f)
                    hasDoneInitalRot = true;
            }
        }
        else
        {
            // goal
            Quaternion dir = Quaternion.LookRotation((yEndTargetPos - playerController.transform.position).normalized, playerController.transform.up);
            // progress
            Quaternion rot = Quaternion.RotateTowards(playerController.transform.rotation, dir, Time.deltaTime * RedirectionSpeed);
            playerController.transform.rotation = rot;
            float diff = Quaternion.Angle(rot, dir);
            if (diff > -1f && diff < 1f) // in position
            {
                EndReposition.Invoke();
            }
        }
    }

    public void OnEndReposition()
    {
        isRepositioning = false;
        hasDoneInitalRot = false;
        hasDoneMovement = false;
        StartAction.Invoke();
    }

    public void OnStartAction()
    {
        // disable character input
    }

    public void OnEndAction()
    {
        // clear targets
        StartTarget = Vector3.zero;
        EndTarget = Vector3.zero;
        EndInteraction();
        currentObject = null;
    }
    #endregion
    // --------
}
