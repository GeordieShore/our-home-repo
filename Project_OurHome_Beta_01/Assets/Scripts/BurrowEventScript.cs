﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public class BurrowEventScript : BaseEventSystemScript
{
    // private float burrowTimerStart = 1.0f;
    // private float burrowTimerEnd = 0.0f;
    // private float burrowTimer = 0.0f;

    // -------- Ethan
    public UnityEvent HalfAction = new UnityEvent();
    // --------

    public override void Start()
    {
        base.Start();
        // burrowTimer = burrowTimerStart;
        HalfAction.AddListener(OnHalfAction);
        StartAction.AddListener(OnStartBurrow);
        EndAction.AddListener(OnEndBurrow);
    }

    public override void Update ()
    {
        base.Update();

        //if (Interacting == true)
        //{
        //    burrowTimer -= Time.deltaTime;

        //    if (burrowTimer <= burrowTimerEnd)
        //    {
        //        EndInteraction();
        //        burrowTimer = burrowTimerStart;
        //    }
        //}
    }

    
    public override void StartInteraction()
    {
        if (currentObject.objectType == InteractableType.BURROW)
        {
			AudioManager.PlaySound (SoundType.WOLFDIGGING);
            // -------- Ethan
            base.StartInteraction();
            StartTarget = transform.position;
            EndTarget = currentObject.interactTransform.transform.position;
            StartReposition.Invoke();
            // --------

            //base.StartInteraction();
            //transform.position = new Vector3(currentObject.interactTransform.transform.position.x, currentObject.interactTransform.transform.position.y, currentObject.interactTransform.transform.position.z);
        }
    }

    //add update interaction and end interaction

    public override void OnTriggerEnter(Collider other)
    {

        //if you enter a push pull object, do this
        InteractableObject iObject = other.GetComponent<InteractableObject>();
        if (iObject && iObject.objectType == InteractableType.BURROW)
        {
            base.OnTriggerEnter(other);
        }
    }

    public override void OnTriggerExit(Collider other)
    {

        //if you enter a push pull object, do this

        InteractableObject iObject = other.GetComponent<InteractableObject>();
        if (iObject && iObject.objectType == InteractableType.BURROW)
        {
            base.OnTriggerExit(other);
        }
    }

    // -------- Ethan
    public void OnHalfAction()
    {
        playerController.startBurrow = false;
        if (currentObject && currentObject.interactTransform)
            transform.position = currentObject.interactTransform.transform.position;
    }

    void OnStartBurrow()
    {
        playerController.LockPhysics = true;
        playerController.LockInput = true;
        playerController.startBurrow = true;
    }

    void OnEndBurrow()
    {
        playerController.LockPhysics = false;
        playerController.LockInput = false;
    }
    // --------

}
