﻿using UnityEngine;
using System.Collections;

public class TriggerAnim : MonoBehaviour {

	public GameObject Activator;
	public GameObject Target;
	public string Value;

	void Start(){
		Value = "activate";
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject == Activator)
			Target.GetComponent<Animator> ().SetBool (Value, true);

	}
}
