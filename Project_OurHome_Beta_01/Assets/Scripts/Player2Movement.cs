﻿using UnityEngine;
using System.Collections;

public class Player2Movement : MonoBehaviour {

    public float force;
    public CharacterController charController;
    public PushPullEventScript pushPullScript;

    private GameObject boxToMove;
    private GameObject currentLocation;

    public bool currentlyInteracting;
    public LayerMask wallLayer = new LayerMask();

    // Use this for initialization
    void Start()
    {
        charController = GetComponent<CharacterController>();
        pushPullScript = GetComponent<PushPullEventScript>();
    }

    // Update is called once per frame
    void Update()
    {

        Vector3 movement = Vector3.zero;

        charController.Move(Vector3.down * force *2 * Time.deltaTime);

        if (Input.GetKey("left"))
        {
            movement += Vector3.left;
        }
        if (Input.GetKey("right"))
        {
            movement += Vector3.right;
        }
        if (Input.GetKey("up"))
        {
            movement += Vector3.forward;
        }
        if (Input.GetKey("down"))
        {
			movement += Vector3.back;
        }

        if (pushPullScript.grabBegun == true)
        {
            
            boxToMove = pushPullScript.currentObject.transform.parent.transform.parent.gameObject;
            currentLocation = pushPullScript.currentObject.gameObject;

            Vector3 actualBoxPosition = (boxToMove.transform.position + Vector3.up * boxToMove.GetComponent<BoxCollider>().bounds.size.y / 2.0f);
            actualBoxPosition.y = currentLocation.transform.position.y;
            Vector3 boxDirection =  currentLocation.transform.position - actualBoxPosition;
            boxDirection = Quaternion.Euler(Vector3.up*90.0f) * boxDirection.normalized;

            Vector3 movementInDirOfBox = boxDirection * Vector3.Dot(boxDirection, movement);

            movement -= movementInDirOfBox;

            Vector3 wallDirection = (currentLocation.transform.position - actualBoxPosition).normalized;

            RaycastHit hit = new RaycastHit();
            if (Physics.Raycast(transform.position, (wallDirection), out hit, 1, wallLayer ))
            {
                Vector3 movementInDirOfWall = wallDirection * Vector3.Dot(wallDirection, movement);

                if(Vector3.Dot(wallDirection, movement) > 0.0f)
                    movement -= movementInDirOfWall;
            }

           


        }


        charController.Move(movement.normalized * force * Time.deltaTime);
    }

}
