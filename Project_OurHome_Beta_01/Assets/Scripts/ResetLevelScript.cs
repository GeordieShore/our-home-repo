﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ResetLevelScript : MonoBehaviour {

	public bool catEnd = false;
	public bool wolfEnd = false;
    public Scene sceneToLoad;

    CreditsScript creditsScript;

	// Use this for initialization
	void Start ()
    {

        creditsScript = transform.GetComponent<CreditsScript>();
	
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKey(KeyCode.Escape))
        {
			SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
		if (Input.GetButtonDown("ResetInput"))
			SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Cat" )
		{
						catEnd = true;
		}

		if (other.tag == "Wolf" )
		{
			wolfEnd = true;
		}

		if (catEnd == true && wolfEnd == true) 
		{
            
			SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
		}

	}


}
