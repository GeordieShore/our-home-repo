﻿using UnityEngine;
using System.Collections;

public class GameManagerScript : MonoBehaviour
{
    public NewSceneSetupScript[] Levels;


    public bool levelChange = false;
    public float levelChangeTimerStart = 1;
    public float levelChangeTimerCurr;

    public int currLevel = 0;

    public GameObject mainCamera;

    // Use this for initialization
    void Start()
    {
        levelChangeTimerCurr = levelChangeTimerStart;
    }

    // Update is called once per frame
    void Update()
    {
        ChangeLevel();
    }



    void ChangeLevel()
    {
        if (levelChange == true)
        {
            //      mainCamera.move
            if (currLevel == 0)
            {
                //load level 1
                currLevel = 1;
            }
            else if ( currLevel == 1)
            {
                //load level 2
                currLevel = 2;
            }
        }
    }
}





