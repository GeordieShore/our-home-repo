﻿using UnityEngine;
using System.Collections;

public class Player1Movement : MonoBehaviour {

    public float force;
    public CharacterController charController;

	// Use this for initialization
	void Start () {
        charController = GetComponent<CharacterController>();
	}
	
	// Update is called once per frame
	void Update ()
    {

        if (Input.GetKey("a"))
        {
            charController.Move(Vector3.left * force * Time.deltaTime);
        }
        if (Input.GetKey("d"))
        {
            charController.Move(Vector3.right * force * Time.deltaTime);
        }
        if (Input.GetKey("w"))
        {
            charController.Move(Vector3.forward * force * Time.deltaTime);
        }
        if (Input.GetKey("s"))
        {
            charController.Move(Vector3.back * force * Time.deltaTime);
        }
    }
}
