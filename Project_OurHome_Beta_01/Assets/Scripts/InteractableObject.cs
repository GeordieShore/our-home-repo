﻿using UnityEngine;
using System.Collections;

public class InteractableObject : MonoBehaviour {

    public InteractableType objectType;
    public GameObject interactTransform;
    private Vector3 dodgyForward;
    public Vector3 DodgyForward
    {
        get { return dodgyForward; }
    }
	// Use this for initialization
	void Start () {
		if (interactTransform) {
			dodgyForward = interactTransform.transform.position - transform.position;
			dodgyForward.y = 0.0f;
			dodgyForward.Normalize ();
		} else
			dodgyForward = Vector3.zero;

    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void StartInteract()
    {


    }
}

public enum InteractableType
{
    PUSHPULL,
    CLIMB,
    JUMP, 
    BURROW,
    ENDLEVEL
}
