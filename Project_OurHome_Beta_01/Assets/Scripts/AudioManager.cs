﻿using UnityEngine;
using System.Collections.Generic;

public class AudioManager : MonoBehaviour {


    public List<SoundEventPair> sounds = new List<SoundEventPair>();

    float windCurrent = 1.0f;
    float windResetTimer = 7.0f;

    float birdSoundCurrent = 3.0f;
    float birdSoundResetTimer = 7.0f;

    float waterCurrent = 5.0f;
    float waterResetTimer = 7.0f;

    float rustlingLeavesCurrent = 7.0f;
    float rustlingLeavesResetTimer = 7.0f;

    [System.Serializable]
    public class SoundEventPair
    {
        public SoundType soundType;
        public AudioSource audioSource;
    }


    private static AudioManager _instance;

    public static AudioManager Instance
    {
        get
        {
            
            //if _instance doesn't exist
            if(_instance == null)
            {
                //create/get instance
                _instance = FindObjectOfType<AudioManager>();

                if(_instance == null)
                {
                    Debug.LogWarning("Need AudioManager attached to object in scene and setup");
                }
            }

            //return instance
            return _instance;
    


        }


    }


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update ()
    {

        ambientSoundTriggers();



        if(Input.GetKeyDown(KeyCode.H))
            AudioManager.PlaySound(SoundType.CATLAND);
    }

    public static void PlaySound(SoundType soundName)
    {
        Instance.Play(soundName);
    }


    //public static void StartSound(SoundType soundName)
    //{
    //    Instance.Play(soundName);
    //}

    //public static void StopSound(SoundType soundName)
    //{
    //    Instance.Play(soundName);
    //}

    private void Play(SoundType soundName)
    {
        SoundEventPair soundPair = sounds.Find(x => x.soundType == soundName);

        if (soundPair == null)
            return;
		if (soundPair.audioSource && !soundPair.audioSource.isPlaying)
			soundPair.audioSource.Play ();
    }
    void ambientSoundTriggers()
    {
        birdSoundCurrent += Time.deltaTime;
        waterCurrent += Time.deltaTime;
        windCurrent += Time.deltaTime;
        rustlingLeavesCurrent += Time.deltaTime;

        if (birdSoundCurrent >= birdSoundResetTimer)
        {
            AudioManager.PlaySound(SoundType.BIRDSOUNDS);
            birdSoundCurrent = 0;
        }
        if (waterCurrent >= waterResetTimer)
        {
            AudioManager.PlaySound(SoundType.WATERRUNNING);
            waterCurrent = 0;
        }
        if (windCurrent >= windResetTimer)
        {
            AudioManager.PlaySound(SoundType.WINDBLOWING);
            windCurrent = 0;
        }
        if (rustlingLeavesCurrent >= rustlingLeavesResetTimer)
        {
            AudioManager.PlaySound(SoundType.RUSTLINGLEAVES);
            rustlingLeavesCurrent = 0;
        }
    }

}


public enum SoundType
{
    CATLAND,
    CATJUMP,
    CATCLIMB,
    CATWALKING,
    WOLFDIGGING,
    WOLFDRAGGING,
    WOLFWALKING,
    AMBIENTMUSIC,
    WINDBLOWING,
    WATERRUNNING,
    BIRDSOUNDS,
    RUSTLINGLEAVES
}