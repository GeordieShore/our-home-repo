﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;


public class SceneChangeScript : MonoBehaviour {

    public bool menuScreen;
	public bool tutorialScreen;
    
    private ScreenFadeScript fadeScript;

    public bool catEnd = false;
    public bool wolfEnd = false;
    public Scene sceneToLoad;
	private float tutorialScreenTimer = 0.0f;
	private float tutorialScreenEnabled = 1.0f;
//    CreditsScript creditsScript;

    // Use this for initialization
    void Start ()
    {
        fadeScript = GetComponent<ScreenFadeScript>();
        //creditsScript = transform.GetComponent<CreditsScript>();

        // Debug.Log("This script is change script");
        LevelBegin();
		

	}
	
	// Update is called once per frame
	void Update ()
    {
		 // Debug.Log (tutorialScreenTimer);
		if (tutorialScreen == true) 
		{
			tutorialScreenTimer += Time.deltaTime;
		}
		if (Input.anyKey == true && tutorialScreen == true && tutorialScreenTimer >= tutorialScreenEnabled)
        {
            // Debug.Log("Key Press");

		   tutorialScreenTimer = 0;
           LevelChange();
        }
	}

    //this is a coroutine, must be an IEnumerator so that i can use Yield
    public void LevelChange()
    {
        StartCoroutine(LevelChangeEnumerator());
    }


    public IEnumerator LevelChangeEnumerator()
    {
        float fadeTime = fadeScript.BeginFade(1);
        yield return new WaitForSeconds(fadeTime);
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
    void LevelBegin()
    {
        fadeScript.BeginFade(-1);
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Cat")
        {
            catEnd = true;
        }

        if (other.tag == "Wolf")
        {
            wolfEnd = true;
        }

        if (catEnd == true && wolfEnd == true)
        {

            StartCoroutine(LevelChangeEnumerator());
        }

    }
    public void ExitProgram()
    {
        Application.Quit();
    }

}
