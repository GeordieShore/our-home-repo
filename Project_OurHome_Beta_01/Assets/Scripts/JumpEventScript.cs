﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class JumpEventScript : BaseEventSystemScript
{
    public List<InteractableObject> jumpPositionList = new List<InteractableObject>();
    
    public override void Update()
    {
        base.Update();
    }

    public override void StartInteraction()
    {

        if (currentObject.objectType == InteractableType.JUMP)
        {
            AudioManager.PlaySound(SoundType.CATJUMP);

            NodeChecker nodeScript = currentObject.GetComponent<NodeChecker>();


            if (nodeScript == null || nodeScript.Useable)
                transform.position = currentObject.interactTransform.transform.position;
        }
    }


    public override void OnTriggerEnter(Collider other)
    {

        //if you enter a push pull object, do this
        InteractableObject iObject = other.GetComponent<InteractableObject>();
       // Debug.Log(iObject.name.ToString());
        if (iObject.objectType == InteractableType.JUMP)
        {

            // base.OnTriggerEnter(other);
            if (other.tag == "Interactable")
            {

                eventPossible = (1 << other.gameObject.layer) == collidableLayer.value;

                if (iObject != null)
                {
                    jumpPositionList.Add(iObject);
                    RecalculateCurrentObject();
                }
            }
        }
    }

    public override void OnTriggerExit(Collider other)
    {

        //if you enter a push pull object, do this

        InteractableObject iObject = other.GetComponent<InteractableObject>();
        if (iObject.objectType == InteractableType.JUMP)
        {
            jumpPositionList.Remove(iObject);
            RecalculateCurrentObject();
            if (jumpPositionList.Count == 0)
            {
                eventPossible = false;
            }
        }

    }

    private void RecalculateCurrentObject()
    {
        if (jumpPositionList.Count == 0)
        {
            currentObject = null;
            return;
        }
        //store float for minDist set to floatMAX
        float minDistance = float.MaxValue;
        //store int for index of closest
        int closestPoint = 0;


        for (int i = 0; i < jumpPositionList.Count; i++)
        {
            //calculate distance to current position (i)
            float currdistance = 0;
            currdistance = (jumpPositionList[i].transform.position - transform.position).sqrMagnitude;
            //if currdistance < mindistance
            if (currdistance < minDistance)
            {

                NodeChecker nodeScript = jumpPositionList[i].GetComponent<NodeChecker>();


                if (nodeScript == null || nodeScript.Useable)
                {
                    minDistance = currdistance;
                    closestPoint = i;
                }
            }
        }

        currentObject = jumpPositionList[closestPoint];

    }

}