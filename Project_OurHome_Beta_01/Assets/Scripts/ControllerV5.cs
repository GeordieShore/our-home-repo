﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ControllerV5 : MonoBehaviour
{
    Rigidbody rigidBody = null;

    //public bool isCat;
    public bool DebugRays = true;

    public string horizontalInputString = "Horizontal";
    public string verticalInputString = "Vertical";

    public PushPullEventScript pushPullScript;

    public MeshCollider cCollider;
    public Animator animator;

    private GameObject boxToMove;
    private GameObject currentLocation;

    [HideInInspector]
    public bool currentlyInteracting;
    public LayerMask wallLayer = new LayerMask();

    public float rotateSpeed = 1.0f;
    [HideInInspector]
    public bool hasInput = false;

    [HideInInspector]
    public Vector3 inputDir = new Vector2();
    [HideInInspector]
    public Vector3 targetDir = new Vector3();
    [HideInInspector]
    public Vector3 nextDir = new Vector3();

    public float walkSpeed = 1;
    public float accelMultiplier = 1;
    public float decelMultiplier = 1;
    public AnimationCurve walkCurve = new AnimationCurve();
    [Range(0, 1)]
    public float curvePos = 0;
    public float nextSpeed = 0;

    [HideInInspector]
    public bool grabLock = false;
    [HideInInspector]
    public Vector3 grabLook = new Vector3();

    public bool LockInput = false;
    bool lockPhysics = false;
    public bool LockPhysics
    {
        get { return lockPhysics; }
        set
        {
            lockPhysics = value;
            rigidBody.useGravity = !lockPhysics;
            rigidBody.isKinematic = lockPhysics;
            cCollider.enabled = !lockPhysics;
        }
    }

    public bool isCat = false;

    bool hasMoved = false;
    bool hasMovedForward = false;

    string[] WolfAnimInfoStrings = new string[] { "isMoving", "isMovingForward", "isGrabbing", "isBurrowing" };
    bool[] WolfAnimInfo;
    int[] WolfAnimInfoHash;

    string[] CatAnimInfoStrings = new string[] { "isMoving", "isClimbingUp", "isClimbingDown", "isLeaping" };
    bool[] CatAnimInfo;
    int[] CatAnimInfoHash;

    [HideInInspector]
    public bool startGrab = false;
    [HideInInspector]
    public bool startBurrow = false;
    [HideInInspector]
    public bool startClimbUp = false;
    [HideInInspector]
    public bool startClimbDown = false;
    [HideInInspector]
    public bool startLeap = false;


    void Start()
    {
        WolfAnimInfo = new bool[WolfAnimInfoStrings.Length];
        WolfAnimInfoHash = new int[WolfAnimInfoStrings.Length];
        CatAnimInfo = new bool[CatAnimInfoStrings.Length];
        CatAnimInfoHash = new int[CatAnimInfoStrings.Length];

        GenerateAnimInfoHash(ref WolfAnimInfoHash, WolfAnimInfoStrings);
        GenerateAnimInfoHash(ref CatAnimInfoHash, CatAnimInfoStrings);

        if (!rigidBody) rigidBody = GetComponent<Rigidbody>();
        if (!cCollider) cCollider = GetComponentInChildren<MeshCollider>();
        if (!pushPullScript) pushPullScript = GetComponent<PushPullEventScript>();
        if (!animator) animator = GetComponent<Animator>();
    }

    void Update()
    {
        inputDir = CalcInputDir(horizontalInputString, verticalInputString);
        targetDir = CalcTargetDir(inputDir);
        hasInput = CheckHasInput(inputDir);

        nextDir = grabLock ? targetDir : CalcNextDir(rotateSpeed, inputDir);
        curvePos = CalcCurvePos(hasInput, curvePos);

        if (isCat)
        {
            UpdateCatAnimState(ref CatAnimInfo);
            ApplyAnimInfo(animator, CatAnimInfo, CatAnimInfoHash);
        }
        else
        {
            UpdateWolfAnimState(ref WolfAnimInfo);
            ApplyAnimInfo(animator, WolfAnimInfo, WolfAnimInfoHash);
        }

        if (DebugRays)
        {
            Debug.DrawRay(RayEmitPos(0.01f), inputDir, Color.red);
            Debug.DrawRay(RayEmitPos(0.01f), nextDir, Color.magenta);
        }
    }

    void FixedUpdate()
    {
        if (hasInput && !LockInput)
        {
            Vector3 rot = grabLock ? grabLook : nextDir;
            rigidBody.MoveRotation(Quaternion.LookRotation(rot));
        }

        // Allow Grab movement
        LimitMovement(ref nextDir);

        if (curvePos != 0)
        {
            nextSpeed = CalcNextSpeed(walkSpeed);
            nextSpeed = ConstrainNextSpeed(nextSpeed, nextDir, transform.position.y + 0.01f);

            Vector3 change = nextSpeed * nextDir;
            if (!LockInput)
                rigidBody.MovePosition(transform.position + change);

            hasMoved = nextSpeed > 0.01f;
            if (hasMoved) hasMovedForward = AngleWithinRange(80);
            else hasMovedForward = false;
        }





		if (hasMoved && isCat == false) 
		{
			if (pushPullScript.grabBegun == true) {
				AudioManager.PlaySound (SoundType.WOLFDRAGGING);
			} 
			else 
			{
				AudioManager.PlaySound (SoundType.WOLFWALKING);
			}
		}
		if (hasMoved && isCat == true) 
		{
			AudioManager.PlaySound (SoundType.CATWALKING);

		}






        if (pushPullScript && pushPullScript.grabBegun == true)
        {
            Vector3 vecFromBoxToPlayer = new Vector3(pushPullScript.currentObject.transform.position.x - transform.position.x, 0, pushPullScript.currentObject.transform.position.z - transform.position.z);
            GrabLookStart(vecFromBoxToPlayer);
        }
        else
        {
            GrabLookEnd();
        }
    }

    public void GrabLookStart(Vector3 _direction)
    {
        grabLook = _direction;
        grabLock = true;
    }

    public void GrabLookEnd()
    {
        targetDir = grabLook;
        grabLock = false;
    }

    void UpdateWolfAnimState(ref bool[] _info)
    {
        _info[0] = hasMoved;
        _info[1] = hasMovedForward;
        _info[2] = grabLock;
        _info[3] = startBurrow;
    }

    void UpdateCatAnimState(ref bool[] _info)
    {
        _info[0] = hasMoved;
        _info[1] = startClimbUp;
        _info[2] = startClimbDown;
        _info[3] = startLeap;
    }

    void ApplyAnimInfo(Animator _animator, bool[] _info, int[] _keys)
    {
        if (_animator)
            for (int i = 0; i < _info.Length; ++i)
                _animator.SetBool(_keys[i], _info[i]);
    }

    void GenerateAnimInfoHash(ref int[] _keys, string[] _names)
    {
        for (int i = 0; i < _names.Length; ++i)
            _keys[i] = Animator.StringToHash(_names[i]);
    }

    bool AngleWithinRange(int _halfArc)
    {
        return Mathf.Abs(Vector3.Angle(nextDir, transform.forward)) < _halfArc;
    }

    Vector3 RayEmitPos(float yOffset)
    {
        return new Vector3(transform.position.x, transform.position.y + yOffset, transform.position.z);
    }

    float CalcCurvePos(bool _increase, float _curvePos)
    {
        if (_increase)
            return Mathf.Min(_curvePos + Time.deltaTime * accelMultiplier, 1);
        else return Mathf.Max(_curvePos - Time.deltaTime * decelMultiplier, 0);
    }

    bool CheckHasInput(Vector3 _inputUnit)
    {
        float inputMag = _inputUnit.magnitude;
        return !(inputMag > -0.01 && inputMag < 0.01);
    }

    float ConstrainNextSpeed(float _distance, Vector3 _direction, float _yMin)
    {
        RaycastHit[] hits = rigidBody.SweepTestAll(_direction, _distance);
        List<RaycastHit> valid = new List<RaycastHit>();

        foreach (RaycastHit hit in hits)
            if (hit.point.y >= _yMin)
                valid.Add(hit);
        if (valid.Count == 1)
            return Mathf.Min(valid[0].distance, _distance);
        else if (valid.Count > 1)
        {
            RaycastHit closest = valid[0];
            for (int i = 1; i < valid.Count; ++i)
            {
                if (valid[i].distance < closest.distance)
                    closest = valid[i];
            }
            return Mathf.Min(closest.distance, _distance);
        }
        else return _distance;
    }

    float CalcNextSpeed(float _walkSpeed)
    {
        return (walkCurve.Evaluate(curvePos) * _walkSpeed) * Time.deltaTime;
    }

    Vector3 CalcInputDir(string _horizontalInputString, string verticalInputString)
    {
        return new Vector3(Input.GetAxisRaw(horizontalInputString), 0.0f, Input.GetAxisRaw(verticalInputString));
    }

    Vector3 CalcTargetDir(Vector3 _inputUnit)
    {
        Vector3 dir = Camera.main.transform.TransformDirection(inputDir);
        return dir;
    }

    Vector3 CalcNextDir(float _rotateSpeed, Vector3 _targetDir)
    {
        return Vector3.RotateTowards(transform.forward, _targetDir, _rotateSpeed * Time.deltaTime, 0.0F);
    }

    void LimitMovement(ref Vector3 _nextDir)
    {
        if (pushPullScript == null || pushPullScript.grabBegun != true)
            return;

        boxToMove = pushPullScript.currentObject.transform.parent.transform.parent.gameObject;

        currentLocation = pushPullScript.currentObject.gameObject;

        Vector3 actualBoxPosition = (boxToMove.transform.position + Vector3.up * boxToMove.GetComponent<BoxCollider>().bounds.size.y / 2.0f);
        actualBoxPosition.y = currentLocation.transform.position.y;
        Vector3 boxDirection = currentLocation.transform.position - actualBoxPosition;
        boxDirection = Quaternion.Euler(Vector3.up * 90.0f) * boxDirection.normalized;

        Vector3 movementInDirOfBox = boxDirection * Vector3.Dot(boxDirection, _nextDir);

        _nextDir -= movementInDirOfBox;

        Vector3 wallDirection = (currentLocation.transform.position - actualBoxPosition).normalized;

        RaycastHit hit = new RaycastHit();
        if (Physics.Raycast(transform.position, (wallDirection), out hit, 1, wallLayer))
        {
            Vector3 movementInDirOfWall = wallDirection * Vector3.Dot(wallDirection, _nextDir);

            if (Vector3.Dot(wallDirection, _nextDir) > 0.0f)
                _nextDir -= movementInDirOfWall;
        }
    }
}
