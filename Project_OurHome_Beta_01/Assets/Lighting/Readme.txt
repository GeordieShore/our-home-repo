To use, make sure 
Edit->Project Settings->Player->Other Settings->Rendering Path
is set to 'Deferred' instead of 'Forward'