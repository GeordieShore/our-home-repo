﻿using UnityEngine;
using System.Collections;
[ExecuteInEditMode]
public class PostEffectScript : MonoBehaviour
{

    public Material PostEffect;

    void OnRenderImage(RenderTexture sourceImage, RenderTexture destinationImage)
    {
        Graphics.Blit(sourceImage, destinationImage, PostEffect);
    }

}
