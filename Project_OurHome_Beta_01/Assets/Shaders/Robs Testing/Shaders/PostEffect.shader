// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Shader created with Shader Forge v1.28 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.28;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:3,spmd:1,trmd:1,grmd:0,uamb:True,mssp:True,bkdf:True,hqlp:False,rprd:True,enco:False,rmgx:True,rpth:1,vtps:1,hqsc:True,nrmq:1,nrsp:0,vomd:1,spxs:False,tesm:0,olmd:1,culm:2,bsrc:0,bdst:1,dpts:6,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:False,qofs:1,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:True,fnfb:True;n:type:ShaderForge.SFN_Final,id:2865,x:32296,y:32910,varname:node_2865,prsc:2|emission-4371-OUT,voffset-4177-OUT;n:type:ShaderForge.SFN_TexCoord,id:6793,x:31734,y:33746,varname:node_6793,prsc:2,uv:0;n:type:ShaderForge.SFN_ProjectionParameters,id:8707,x:31734,y:33981,varname:node_8707,prsc:2;n:type:ShaderForge.SFN_RemapRange,id:9496,x:31891,y:33746,varname:node_9496,prsc:2,frmn:0,frmx:1,tomn:-1,tomx:1|IN-6793-UVOUT;n:type:ShaderForge.SFN_Append,id:6538,x:31929,y:33958,varname:node_6538,prsc:2|A-5896-OUT,B-8707-SGN;n:type:ShaderForge.SFN_Vector1,id:5896,x:31734,y:33910,varname:node_5896,prsc:2,v1:1;n:type:ShaderForge.SFN_Multiply,id:4177,x:32124,y:33828,varname:node_4177,prsc:2|A-9496-OUT,B-6538-OUT;n:type:ShaderForge.SFN_Tex2d,id:9507,x:31432,y:33059,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:node_9507,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Dot,id:740,x:31553,y:33202,varname:node_740,prsc:2,dt:0|A-218-OUT,B-1273-OUT;n:type:ShaderForge.SFN_TexCoord,id:5653,x:30454,y:33155,varname:node_5653,prsc:2,uv:0;n:type:ShaderForge.SFN_TexCoord,id:5408,x:30443,y:33425,varname:node_5408,prsc:2,uv:0;n:type:ShaderForge.SFN_Slider,id:218,x:31123,y:33202,ptovrint:False,ptlb:ExposureIntensity,ptin:_ExposureIntensity,varname:node_218,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:3;n:type:ShaderForge.SFN_Multiply,id:578,x:31724,y:33171,varname:node_578,prsc:2|A-9507-RGB,B-740-OUT;n:type:ShaderForge.SFN_Multiply,id:1273,x:31377,y:33294,varname:node_1273,prsc:2|A-218-OUT,B-1417-OUT;n:type:ShaderForge.SFN_RemapRangeAdvanced,id:5889,x:30922,y:33155,varname:node_5889,prsc:2|IN-5653-UVOUT,IMIN-950-OUT,IMAX-4389-OUT,OMIN-612-OUT,OMAX-204-OUT;n:type:ShaderForge.SFN_RemapRangeAdvanced,id:333,x:30923,y:33425,varname:node_333,prsc:2|IN-5408-UVOUT,IMIN-8297-OUT,IMAX-657-OUT,OMIN-9881-OUT,OMAX-7588-OUT;n:type:ShaderForge.SFN_Slider,id:950,x:30584,y:33207,ptovrint:False,ptlb:Exposure1,ptin:_Exposure1,varname:node_950,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-1,cur:-0.5,max:0;n:type:ShaderForge.SFN_Slider,id:8297,x:30584,y:33481,ptovrint:False,ptlb:Exposure2,ptin:_Exposure2,varname:node_8297,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_TexCoord,id:2182,x:31012,y:32803,varname:node_2182,prsc:2,uv:0;n:type:ShaderForge.SFN_TexCoord,id:1762,x:31012,y:32982,varname:node_1762,prsc:2,uv:0;n:type:ShaderForge.SFN_RemapRange,id:3107,x:31181,y:32803,varname:node_3107,prsc:2,frmn:0,frmx:1,tomn:-1,tomx:1|IN-2182-UVOUT;n:type:ShaderForge.SFN_RemapRange,id:106,x:31181,y:32982,varname:node_106,prsc:2,frmn:0,frmx:1,tomn:-1,tomx:1|IN-1762-UVOUT;n:type:ShaderForge.SFN_Multiply,id:8363,x:31400,y:32883,varname:node_8363,prsc:2|A-3107-OUT,B-106-OUT;n:type:ShaderForge.SFN_Dot,id:1417,x:31142,y:33284,varname:node_1417,prsc:2,dt:0|A-5889-OUT,B-333-OUT;n:type:ShaderForge.SFN_Vector1,id:4389,x:30741,y:33275,varname:node_4389,prsc:2,v1:0.4;n:type:ShaderForge.SFN_Vector1,id:612,x:30741,y:33329,varname:node_612,prsc:2,v1:1;n:type:ShaderForge.SFN_Vector1,id:204,x:30741,y:33390,varname:node_204,prsc:2,v1:0.4;n:type:ShaderForge.SFN_Vector1,id:657,x:30741,y:33548,varname:node_657,prsc:2,v1:0.4;n:type:ShaderForge.SFN_Vector1,id:9881,x:30741,y:33602,varname:node_9881,prsc:2,v1:1;n:type:ShaderForge.SFN_Vector1,id:7588,x:30741,y:33663,varname:node_7588,prsc:2,v1:0.4;n:type:ShaderForge.SFN_Dot,id:4433,x:31592,y:32883,varname:node_4433,prsc:2,dt:0|A-3985-OUT,B-8363-OUT;n:type:ShaderForge.SFN_Slider,id:3985,x:31400,y:32802,ptovrint:False,ptlb:Vignette,ptin:_Vignette,varname:node_3985,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_OneMinus,id:2166,x:31757,y:32858,varname:node_2166,prsc:2|IN-4433-OUT;n:type:ShaderForge.SFN_Multiply,id:4371,x:31984,y:32900,varname:node_4371,prsc:2|A-969-OUT,B-578-OUT;n:type:ShaderForge.SFN_Power,id:969,x:31924,y:32701,varname:node_969,prsc:2|VAL-2166-OUT,EXP-4684-OUT;n:type:ShaderForge.SFN_Slider,id:4684,x:31544,y:32699,ptovrint:False,ptlb:Vignette Threshold,ptin:_VignetteThreshold,varname:node_4684,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:5,max:5;proporder:9507-218-950-8297-3985-4684;pass:END;sub:END;*/

Shader "" {
    Properties {
        _MainTex ("MainTex", 2D) = "white" {}
        _ExposureIntensity ("ExposureIntensity", Range(0, 3)) = 1
        _Exposure1 ("Exposure1", Range(-1, 0)) = -0.5
        _Exposure2 ("Exposure2", Range(0, 1)) = 1
        _Vignette ("Vignette", Range(0, 1)) = 1
        _VignetteThreshold ("Vignette Threshold", Range(0, 5)) = 5
    }
    SubShader {
        Tags {
            "Queue"="Geometry+1"
            "RenderType"="Opaque"
        }
        Pass {
            Name "DEFERRED"
            Tags {
                "LightMode"="Deferred"
            }
            Cull Off
            ZTest Always
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_DEFERRED
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile ___ UNITY_HDR_ON
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float _ExposureIntensity;
            uniform float _Exposure1;
            uniform float _Exposure2;
            uniform float _Vignette;
            uniform float _VignetteThreshold;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                v.vertex.xyz = float3(((o.uv0*2.0+-1.0)*float2(1.0,_ProjectionParams.r)),0.0);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = v.vertex;
                return o;
            }
            void frag(
                VertexOutput i,
                out half4 outDiffuse : SV_Target0,
                out half4 outSpecSmoothness : SV_Target1,
                out half4 outNormal : SV_Target2,
                out half4 outEmission : SV_Target3,
                float facing : VFACE )
            {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                i.normalDir = normalize(i.normalDir);
                i.normalDir *= faceSign;
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float3 viewReflectDirection = reflect( -viewDirection, normalDirection );
////// Lighting:
////// Emissive:
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float node_612 = 1.0;
                float node_9881 = 1.0;
                float3 emissive = (pow((1.0 - dot(_Vignette,((i.uv0*2.0+-1.0)*(i.uv0*2.0+-1.0)))),_VignetteThreshold)*(_MainTex_var.rgb*dot(_ExposureIntensity,(_ExposureIntensity*dot((node_612 + ( (i.uv0 - _Exposure1) * (0.4 - node_612) ) / (0.4 - _Exposure1)),(node_9881 + ( (i.uv0 - _Exposure2) * (0.4 - node_9881) ) / (0.4 - _Exposure2)))))));
                float3 finalColor = emissive;
                outDiffuse = half4( 0, 0, 0, 1 );
                outSpecSmoothness = half4(0,0,0,0);
                outNormal = half4( normalDirection * 0.5 + 0.5, 1 );
                outEmission = half4( (pow((1.0 - dot(_Vignette,((i.uv0*2.0+-1.0)*(i.uv0*2.0+-1.0)))),_VignetteThreshold)*(_MainTex_var.rgb*dot(_ExposureIntensity,(_ExposureIntensity*dot((node_612 + ( (i.uv0 - _Exposure1) * (0.4 - node_612) ) / (0.4 - _Exposure1)),(node_9881 + ( (i.uv0 - _Exposure2) * (0.4 - node_9881) ) / (0.4 - _Exposure2))))))), 1 );
                #ifndef UNITY_HDR_ON
                    outEmission.rgb = exp2(-outEmission.rgb);
                #endif
            }
            ENDCG
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Cull Off
            ZTest Always
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float _ExposureIntensity;
            uniform float _Exposure1;
            uniform float _Exposure2;
            uniform float _Vignette;
            uniform float _VignetteThreshold;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                v.vertex.xyz = float3(((o.uv0*2.0+-1.0)*float2(1.0,_ProjectionParams.r)),0.0);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = v.vertex;
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                i.normalDir = normalize(i.normalDir);
                i.normalDir *= faceSign;
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float3 viewReflectDirection = reflect( -viewDirection, normalDirection );
////// Lighting:
////// Emissive:
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float node_612 = 1.0;
                float node_9881 = 1.0;
                float3 emissive = (pow((1.0 - dot(_Vignette,((i.uv0*2.0+-1.0)*(i.uv0*2.0+-1.0)))),_VignetteThreshold)*(_MainTex_var.rgb*dot(_ExposureIntensity,(_ExposureIntensity*dot((node_612 + ( (i.uv0 - _Exposure1) * (0.4 - node_612) ) / (0.4 - _Exposure1)),(node_9881 + ( (i.uv0 - _Exposure2) * (0.4 - node_9881) ) / (0.4 - _Exposure2)))))));
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
        Pass {
            Name "Meta"
            Tags {
                "LightMode"="Meta"
            }
            Cull Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_META 1
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #include "UnityMetaPass.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float _ExposureIntensity;
            uniform float _Exposure1;
            uniform float _Exposure2;
            uniform float _Vignette;
            uniform float _VignetteThreshold;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                v.vertex.xyz = float3(((o.uv0*2.0+-1.0)*float2(1.0,_ProjectionParams.r)),0.0);
                o.pos = UnityMetaVertexPosition(v.vertex, v.texcoord1.xy, v.texcoord2.xy, unity_LightmapST, unity_DynamicLightmapST );
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : SV_Target {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                UnityMetaInput o;
                UNITY_INITIALIZE_OUTPUT( UnityMetaInput, o );
                
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float node_612 = 1.0;
                float node_9881 = 1.0;
                o.Emission = (pow((1.0 - dot(_Vignette,((i.uv0*2.0+-1.0)*(i.uv0*2.0+-1.0)))),_VignetteThreshold)*(_MainTex_var.rgb*dot(_ExposureIntensity,(_ExposureIntensity*dot((node_612 + ( (i.uv0 - _Exposure1) * (0.4 - node_612) ) / (0.4 - _Exposure1)),(node_9881 + ( (i.uv0 - _Exposure2) * (0.4 - node_9881) ) / (0.4 - _Exposure2)))))));
                
                float3 diffColor = float3(0,0,0);
                float specularMonochrome;
                float3 specColor;
                diffColor = DiffuseAndSpecularFromMetallic( diffColor, 0, specColor, specularMonochrome );
                o.Albedo = diffColor;
                
                return UnityMetaFragment( o );
            }
            ENDCG
        }
    }
    CustomEditor "ShaderForgeMaterialInspector"
}
