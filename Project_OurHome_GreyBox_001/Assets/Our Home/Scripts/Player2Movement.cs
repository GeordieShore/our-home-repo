﻿using UnityEngine;
using System.Collections;

public class Player2Movement : MonoBehaviour {

    public float force;
    public CharacterController charController;
    public PushPullEventScript pushPullScript;

    private GameObject boxToMove;
    private GameObject currentLocation;

    public bool currentlyInteracting;


    // Use this for initialization
    void Start()
    {
        charController = GetComponent<CharacterController>();
        pushPullScript = GetComponent<PushPullEventScript>();
    }

    // Update is called once per frame
    void Update()
    {



        Vector3 movement = Vector3.zero;

        charController.Move(Vector3.down * force *2 * Time.deltaTime);

        if (Input.GetKey("left"))
        {
            movement += Vector3.left;
        }
        if (Input.GetKey("right"))
        {
            movement += Vector3.right;
        }
        if (Input.GetKey("up"))
        {
            movement += Vector3.forward;
        }
        if (Input.GetKey("down"))
        {
            movement += Vector3.back;
        }

        if (pushPullScript.grabBegun == true)
        {
            
            boxToMove = pushPullScript.currentObject.transform.parent.gameObject;
            currentLocation = pushPullScript.currentObject.gameObject;

            Vector3 actualBoxPosition = (boxToMove.transform.position + Vector3.up * boxToMove.GetComponent<BoxCollider>().bounds.size.y / 2.0f);
            actualBoxPosition.y = currentLocation.transform.position.y;
            Vector3 boxDirection =  currentLocation.transform.position - actualBoxPosition;
            boxDirection = Quaternion.Euler(Vector3.up*90.0f) * boxDirection.normalized;
            Debug.Log(boxDirection + "this is my box it is my box no one elses box");

            Vector3 movementInDirOfBox = boxDirection * Vector3.Dot(boxDirection, movement);
            Debug.Log(movementInDirOfBox + "movement allowed");

            movement -= movementInDirOfBox;
           
        }


        Debug.Log(movement + "is the movement");
        charController.Move(movement.normalized * force * Time.deltaTime);
    }

}
