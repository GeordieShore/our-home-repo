﻿using UnityEngine;
using System.Collections;

public class PushPullEventScript : BaseEventSystemScript
{
    Vector3 posThisFrame;
    Vector3 posLastFrame;
    Vector3 characterMovement;
    Vector3 vecFromBoxToPlayer;

    public bool grabBegun;

    public LayerMask BoxCollideLayer;

    CharacterController charController;

    float collisionKnockBack = 5;
    float boxCollisionTimer = 0.6f;

   public bool touchingBox = false;


public float pushPullPower = -1.0f;

	// Use this for initialization
	void Start () {
        charController = GetComponent<CharacterController>();
        posThisFrame = this.transform.position;
        posLastFrame = this.transform.position;
        grabBegun = false;
    }

    // Update is called once per frame
    public override void Update()
    {

        posThisFrame = this.transform.position;
        base.Update();
        characterMovement = posThisFrame - posLastFrame;
        posLastFrame = this.transform.position;



    }

    

    public override void StartInteraction()
    {
        //if you CAN interact (check objectType) //if (currentObject.objectType == InteractableType.PUSHPULL )
            base.StartInteraction();
            grabBegun = true;
        
    }

    Vector3 crateActualNewPosition;
    public override void UpdateInteraction()
    {
        if(Interacting)
        {
            Vector3 grabSphereTempVec = currentObject.interactTransform.transform.position;
            grabSphereTempVec.y = transform.position.y;
            transform.position = grabSphereTempVec;

            // check to see whether the player is colliding with the box

            // if collision  == true 

            //raycast from center of player to center of box
            if (touchingBox == true)
            {
                boxCollisionTimer -= Time.deltaTime;
                if (boxCollisionTimer <= 0)
                {
                    touchingBox = false;
                }
                vecFromBoxToPlayer = new Vector3 (transform.position.x - currentObject.transform.position.x, 0, transform.position.z - currentObject.transform.position.z);

                


            }

            Transform crate = currentObject.transform.parent;
            Rigidbody crateRigidbody = crate.GetComponent<Rigidbody>();

            //calculate the new position of the crate based on it's current position and player movement that frame
            Vector3 newPosition = crate.position + characterMovement;


            //store reference to crate boxcollider
            BoxCollider crateBoxCollider = crate.GetComponent<BoxCollider>();
            //store Vector3 for crateActualNewPosition 

            crateActualNewPosition = newPosition + Vector3.up * crateBoxCollider.bounds.size.y / 2.0f;
            //cast an overlap box at the newposition to see what's there. 
            Collider[] objects = Physics.OverlapBox(crateActualNewPosition , crate.GetComponent<BoxCollider>().bounds.extents * 0.15f, crate.rotation, BoxCollideLayer);

            //if no objects, great, just move the box.
            if (objects.Length == 0)
            {
                Vector3 tempVec = characterMovement;
                tempVec.y = 0;
                crateRigidbody.MovePosition(crate.position + tempVec);

            }
            else
            {
                Debug.Log(objects[0].name);
            }


        }
    }

    public override void EndInteraction()
    {
        base.EndInteraction();
        grabBegun = false;
    }

    public override void OnTriggerEnter(Collider other)
    {

        //if you enter a push pull object, do this
        InteractableObject iObject = other.GetComponent<InteractableObject>();
        if (iObject.objectType == InteractableType.PUSHPULL)
        {
            base.OnTriggerEnter(other);
        }
    }

    public override void OnTriggerExit(Collider other)
    {

        //if you enter a push pull object, do this

        InteractableObject iObject = other.GetComponent<InteractableObject>();
        if (iObject.objectType == InteractableType.PUSHPULL)
        {
            base.OnTriggerExit(other);
        }

    }

    void onCollisionEnter(Collider other)
    {
        if (other.tag == "Pushable")
        {
            touchingBox = true;
        }
    }

    void onCollisionExit(Collider other)
    {
        touchingBox = false;
    }

    void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if (hit.gameObject.tag == "Pushable")
        {
            touchingBox = true;
        }
    }

    void OnDrawGizmos()
    {



      //  Gizmos.DrawSphere(crateActualNewPosition, 0.2f);
    }
}
