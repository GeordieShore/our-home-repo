﻿using UnityEngine;
using System.Collections;

public class CameraArmScript : MonoBehaviour {
    public GameObject pos1;
    public GameObject pos2;
    public float zOffset;
    public bool useZOffset;
    private Vector3 desiredPos;

	// Use this for initialization
	void Start ()
    {
	
	}
	
	// Update is called once per frame
	void Update ()
    {
        //this check decides whether or not to move the camera on the z axis
        if (useZOffset == true)
        {
            desiredPos = new Vector3(((pos1.transform.position.x + pos2.transform.position.x) / 2), transform.position.y, (pos1.transform.position.z + pos2.transform.position.z / 2) - zOffset);
        }
        else if (useZOffset == false)
        {
            desiredPos = new Vector3(((pos1.transform.position.x + pos2.transform.position.x) / 2), transform.position.y, transform.position.z);

        }

        
       transform.position = desiredPos;

    }
}
