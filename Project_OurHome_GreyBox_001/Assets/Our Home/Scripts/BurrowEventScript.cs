﻿using UnityEngine;
using System.Collections;

public class BurrowEventScript : BaseEventSystemScript
{
    private float burrowTimerStart = 1.0f;
    private float burrowTimerEnd = 0.0f;
    private float burrowTimer = 0.0f;
    // Use this for initialization
    void Start () {
        burrowTimer = burrowTimerStart;
    }

    // Update is called once per frame
    public override void Update ()
    {
        base.Update();
        if (Interacting == true)
        {
            burrowTimer -= Time.deltaTime;

            if (burrowTimer <= burrowTimerEnd)
            {
                EndInteraction();
                burrowTimer = burrowTimerStart;
            }
        }
    }

    
    public override void StartInteraction()
    {
        if (currentObject.objectType == InteractableType.BURROW)
        { 
            base.StartInteraction();
            transform.position = new Vector3(currentObject.interactTransform.transform.position.x, currentObject.interactTransform.transform.position.y, transform.position.z);
        }
    }

    //add update interaction and end interaction

    public override void OnTriggerEnter(Collider other)
    {

        //if you enter a push pull object, do this
        InteractableObject iObject = other.GetComponent<InteractableObject>();
        if (iObject.objectType == InteractableType.BURROW)
        {
            base.OnTriggerEnter(other);
        }
    }

    public override void OnTriggerExit(Collider other)
    {

        //if you enter a push pull object, do this

        InteractableObject iObject = other.GetComponent<InteractableObject>();
        if (iObject.objectType == InteractableType.BURROW)
        {
            base.OnTriggerExit(other);
        }

    }
}
