﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ResetLevelScript : MonoBehaviour {

	public bool catEnd = false;
	public bool wolfEnd = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Cat" )
		{
						catEnd = true;
		}

		if (other.tag == "Wolf" )
		{
			wolfEnd = true;
		}

		if (catEnd == true && wolfEnd == true) 
		{
				Application.LoadLevel(0);
		}

	}


}
