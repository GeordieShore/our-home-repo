﻿using UnityEngine;
using System.Collections;

public class BaseEventSystemScript : MonoBehaviour
{

    public bool eventPossible;
    public bool triggerEvent;
    public bool triggerOnce;
    public InteractableObject currentObject;
    public bool isCat;
    public LayerMask collidableLayer;
    public Player2Movement playerController;
    public bool Interacting = false;
    
    // Use this for initialization
    void Start()
    {
        //need to get this and set it in the unity window
         playerController = GetComponent<Player2Movement>();
    }

    // Update is called once per frame
    public virtual void Update()
    {
        //this controls whether or not push or any of the other mechanics are being used.
        if (triggerOnce == true)
            PlayerMovementAction();
        else if (triggerOnce == false)
            ObjectMovementAction();
    }



    public virtual void PlayerMovementAction()
    {
        if (isCat == true)
        {
            if (eventPossible == true && Input.GetKeyDown(KeyCode.E))
            {
                //do things with currentObject if it's not null
                if (currentObject != null)
                {
                    StartInteraction();
                }
            }
        }
        else if (isCat == false)
        {
            if (eventPossible == true && Input.GetKeyDown(KeyCode.LeftControl))
            {
                //do things with currentObject if it's not null
                if (currentObject != null)
                {
                    StartInteraction();
                }
            }
        }
    }

    void ObjectMovementAction()
    {
        if (isCat == true)
        {
            if (eventPossible == true && Input.GetKey(KeyCode.E))
            {
                //do things with currentObject if it's not null
                if (currentObject != null)
                {
                    StartInteraction();
                }

            }
        }
        else if (isCat == false)
        {
            if (eventPossible == true && Input.GetKey(KeyCode.LeftControl))
            {
                Debug.Log("block movement");
                //do things with currentObject if it's not null
                if (currentObject != null)
                {
                    //must make sure to start update and end the interactions
                    if (Interacting == false)
                    {
                        if (playerController.currentlyInteracting == false)
                        StartInteraction();
                    }
                        UpdateInteraction();
                }

            }
            else
            {

                if (Interacting == true)
                    EndInteraction();
            }
           
        }
    }

    public virtual void StartInteraction()
    {
        playerController.currentlyInteracting = true;
        Interacting = true;
    }
    public virtual void UpdateInteraction()
    {

    }

    public virtual void EndInteraction()
    {
        playerController.currentlyInteracting = false;
        Interacting = false;
    }

    public virtual void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Interactable")
        {

            eventPossible = (1 << other.gameObject.layer) == collidableLayer.value;

            InteractableObject iObject = other.GetComponent<InteractableObject>();
            if (iObject != null)
            {
                currentObject = iObject;
            }
        }

    }

    public virtual void OnTriggerExit(Collider other)
    {
        InteractableObject iObject = other.GetComponent<InteractableObject>();

        if (other.GetComponent<InteractableObject>() == currentObject)
        {
            eventPossible = false;
        }
    }


}
