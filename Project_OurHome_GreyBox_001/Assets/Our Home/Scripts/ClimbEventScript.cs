﻿using UnityEngine;
using System.Collections;

public class ClimbEventScript : BaseEventSystemScript {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
public override void Update ()
    {
        base.Update();
	}

    public override void StartInteraction()
    {
        if (currentObject.objectType == InteractableType.CLIMB)
        {
            transform.position = currentObject.interactTransform.transform.position;
        }
    }


    public override void OnTriggerEnter(Collider other)
    {

        //if you enter a push pull object, do this
        InteractableObject iObject = other.GetComponent<InteractableObject>();
        if (iObject.objectType == InteractableType.CLIMB)
        {
            base.OnTriggerEnter(other);
        }
    }

    public override void OnTriggerExit(Collider other)
    {

        //if you enter a push pull object, do this

        InteractableObject iObject = other.GetComponent<InteractableObject>();
        if (iObject.objectType == InteractableType.CLIMB)
        {
            base.OnTriggerExit(other);
        }

    }
}
