﻿using UnityEngine;
using System.Collections;

public class CameraScript : MonoBehaviour {
    public GameObject PrefferedPos;
    private Vector3 desiredPos;
    private float lerpSpeed = 1;
    public float smoothTime = 1000F;
    private float yVelocity = 0.0F;
    private Vector3 velocity = Vector3.zero;

    // Use this for initialization
    void Start () {
	}
	
	// Update is called once per frame
	void LateUpdate ()
    {


        //lerpCameraMovement();

        //smoothDampCameraMovement();

        vectorsmoothDampCameraMovement();
    }



    void lerpCameraMovement()
    {
        desiredPos = new Vector3(PrefferedPos.transform.position.x, PrefferedPos.transform.position.y, PrefferedPos.transform.position.z);
        transform.position = Vector3.Lerp(transform.position, desiredPos, Time.deltaTime * lerpSpeed);
    }

    void smoothDampCameraMovement()
    {
        float newPosition = Mathf.SmoothDamp(transform.position.x, PrefferedPos.transform.position.x, ref yVelocity, smoothTime);
        transform.position = new Vector3( newPosition, transform.position.y, transform.position.z);
    }

    void vectorsmoothDampCameraMovement()
    {
        desiredPos = new Vector3(PrefferedPos.transform.position.x, PrefferedPos.transform.position.y, PrefferedPos.transform.position.z);
        transform.position = Vector3.SmoothDamp(transform.position, desiredPos, ref velocity, smoothTime);

    }
}
