﻿using UnityEngine;
using System.Collections;

public class EventTriggerScript : MonoBehaviour {


    public bool eventPossible;
    public bool triggerEvent;
    public Vector3 finalLocation;

    private InteractableObject currentObject;
	// Use this for initialization
	void Start ()
    {
	}
	
	// Update is called once per frame
	void Update ()
    {


       



        if (eventPossible == true && Input.GetKeyDown(KeyCode.E))
        {
            //do things with currentObject if it's not null
            if(currentObject != null)
            {
                if (currentObject.objectType == InteractableType.CLIMB)
                {
                    currentObject.StartInteract();
                    transform.position = currentObject.interactTransform.transform.position;
                }
            }

            //transform.position = finalLocation;
        }




	}



    // check collider function
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Interactable")
        {
            eventPossible = true;

            InteractableObject iObject = other.GetComponent<InteractableObject>();

            if(iObject != null)
            {

                currentObject = iObject;
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        eventPossible = false;
    }

   
}
