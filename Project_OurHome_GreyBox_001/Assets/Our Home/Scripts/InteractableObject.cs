﻿using UnityEngine;
using System.Collections;

public class InteractableObject : MonoBehaviour {

    public InteractableType objectType;
    public GameObject interactTransform;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void StartInteract()
    {


    }
}

public enum InteractableType
{
    PUSHPULL,
    CLIMB,
    JUMP, 
    BURROW,
    ENDLEVEL
}
