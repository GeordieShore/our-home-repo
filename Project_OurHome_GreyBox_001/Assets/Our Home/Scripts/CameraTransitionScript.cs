﻿using UnityEngine;
using System.Collections;

public class CameraTransitionScript : MonoBehaviour {

    public bool beginTransition = false;
    public bool halfWay = false;
    public bool finalLocation = false;

    public GameObject middleLocation;
    public GameObject endLocation;
    public float cameraTurnSpeed = 1.0f;



    public float smoothTime = 1000F;
    private Vector3 velocity = Vector3.zero;

    // Use this for initialization
    void Start ()
    {
	
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Space) == true)
        {
            if (beginTransition == false)
                beginTransition = true;
            else
                beginTransition = false;
        }

        if (beginTransition == true && halfWay == false)
        {
            PointRotationAtObject(middleLocation);
            moveTowardsLocation(middleLocation);
        }
        if (halfWay == true && finalLocation == false)
        {
            moveTowardsLocation(endLocation);
            transform.rotation = Quaternion.LookRotation(middleLocation.transform.position - transform.position);

        }

    }

    void PointRotationAtObject(GameObject a_objectToPointAt)
    {
        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(a_objectToPointAt.transform.position - transform.position), Time.deltaTime * cameraTurnSpeed);
    }

    void SetUpRotationForScene(GameObject a_finalRotation)
    {
       transform.rotation.SetLookRotation(a_finalRotation.transform.rotation.eulerAngles);

    }

    void moveTowardsLocation(GameObject a_locationToMoveTo)
    {
        vectorsmoothDampCameraMovement(a_locationToMoveTo.transform.position);
    }


    

    void vectorsmoothDampCameraMovement(Vector3 a_prefferedPos)
    {
        Vector3 desiredPos = new Vector3(a_prefferedPos.x, a_prefferedPos.y, a_prefferedPos.z);
        transform.position = Vector3.SmoothDamp(transform.position, desiredPos, ref velocity, smoothTime);
    }


    void setBeginTransition()
    {
        beginTransition = true;
    }


    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Door")
        {
            Debug.Log("doorcollision");

            halfWay = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Door")
        {
        }
    }
}
